﻿using UnityEngine;

public class DeathSound : MonoBehaviour
{
    AudioSource audioS;
    public PlayerCharacter player;
    public AudioClip deathSong;

    void Start()
    {
        audioS = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (player.hp == 0)
        {  
            audioS.PlayOneShot(deathSong);
            player.hp = 1;
        }
    }
}
