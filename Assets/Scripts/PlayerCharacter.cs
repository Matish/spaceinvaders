﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerCharacter : MonoBehaviour
{
    public MapLimits Limits;
    public int movementSpeed;
    public int hp;
    public int score;
    public GameObject bullet;
    public GameObject particleEffect;
    public Transform pos1;
    public Transform posL;
    public Transform posR;
    public AudioClip shotSound;
    public AudioClip lvlSound;
    public AudioClip deathSong;
    public Text scoreText;
    public Text highScoreText;
    public Text livesText;
    public Text gameOverText;
    public float shootPower;
    private bool isPaused;
    private AudioSource audioS;
    private int highScore;
    private int power;

    void movingLeft()
    {
         if (Input.GetKey(KeyCode.LeftArrow))
         {
             transform.Translate(Vector3.right * -movementSpeed * Time.deltaTime);
         }
    }

    void movingRight()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
        }
    }

    void movingUp()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.up * movementSpeed * Time.deltaTime);
        }
    }

    void movingDown()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.down * movementSpeed * Time.deltaTime);
        }
    }

    void checkPosition()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, Limits.minimumX, Limits.maximumX), 
            Mathf.Clamp(transform.position.y, Limits.minimumY, Limits.maximumY), 0.0f);
    }

    void shooting()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            audioS.PlayOneShot(shotSound);

            switch(power)
            {
                case 1:
                {
                    GameObject newBullet = Instantiate(bullet, pos1.position, transform.rotation);
                    newBullet.GetComponent<Rigidbody>().velocity = Vector3.up * shootPower; 
                }
                break;

                case 2:
                {
                    GameObject bullet1 = Instantiate(bullet, posR.position, transform.rotation);
                    bullet1.GetComponent<Rigidbody>().velocity =  Vector3.up *  shootPower;
                    GameObject bullet2 = Instantiate(bullet, posL.position, transform.rotation);
                    bullet2.GetComponent<Rigidbody>().velocity =  Vector3.up * shootPower;
                }
                break;

                case 3:
                {
                    GameObject bullet1 = Instantiate(bullet, posL.position, transform.rotation);
                    bullet1.GetComponent<Rigidbody>().velocity =  Vector3.up * shootPower;
                    GameObject bullet2 = Instantiate(bullet, posR.position, transform.rotation);
                    bullet2.GetComponent<Rigidbody>().velocity =  Vector3.up * shootPower;
                    GameObject bullet3 = Instantiate(bullet, pos1.position, transform.rotation);
                    bullet3.GetComponent<Rigidbody>().velocity =  Vector3.up * shootPower;
                }
                break;

                default:
                {
                    GameObject newBullet = Instantiate(bullet, pos1.position, transform.rotation);
                    newBullet.GetComponent<Rigidbody>().velocity = Vector3.up * shootPower;
                }
                break;
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "powerUp")
        {
            if (power < 3)
            {
                power++;
            }

            Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "powerDown")
        {
            if (power> 1)
            {
                power--;
            }
                
            Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "enemyBullet")
        {
            hp--;
            Instantiate(particleEffect, transform.position, transform.rotation);
            Destroy(col.gameObject);
        }
    }

    void Movement()
    {
        movingLeft();
        movingRight();
        movingUp();
        movingDown();
    }

    void Start()
    {
        score = 0;
        power = 1;
        gameOverText.enabled = false;
        audioS = GetComponent<AudioSource>();
        audioS.PlayOneShot(lvlSound);

       if (!PlayerPrefs.HasKey("highscore"))
        {
            highScore = 0;
            PlayerPrefs.SetInt("highscore", highScore);
        }
    }

    void Update()
    {
        livesText.text = hp.ToString();
        highScore = PlayerPrefs.GetInt("highscore");
        scoreText.text = score.ToString();
        highScoreText.text = PlayerPrefs.GetInt("highscore").ToString();

        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("highscore", highScore);
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            audioS.Pause();
        }
       
        Movement();

        if (hp <=0)
        {
            audioS.Stop();
            gameOverText.enabled = true;
            Destroy(gameObject);
        }

        checkPosition();
        shooting();
    }
}