﻿[System.Serializable]
public class MapLimits
{
    public int minimumX;
    public int maximumX;
    public int minimumY;
    public int maximumY;
}