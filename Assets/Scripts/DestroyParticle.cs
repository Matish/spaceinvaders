﻿using UnityEngine;

public class DestroyParticle : MonoBehaviour
{
    public float timer;
    public GameObject particle;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            Destroy(gameObject);
        }
    }
}