﻿using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed;
    public float changeTimer;
    public float shootPower;
    public int scoreReward;
    public int hp;
    public bool directionSwitch;
    public bool canShoot;
    public GameObject particleEffect;
    public GameObject bullet;
    public GameObject powerUp;
    public GameObject powerDown;
    public Transform shootingPosition;
    public MapLimits Limits;
    private Rigidbody rig;
    private float shootTimer;
    private float maxShootTimer;
    private float maxTimer;

    void checkPosition()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, Limits.minimumX, Limits.maximumX),
            Mathf.Clamp(transform.position.y, Limits.minimumY, Limits.maximumY), 0.0f);
    }

    void Movement()
    {
        if (directionSwitch)
        {
            rig.velocity = new Vector3(speed * Time.deltaTime, -speed * Time.deltaTime, 0);
        }
        else
        {
            rig.velocity = new Vector3(-speed * Time.deltaTime, -speed * Time.deltaTime, 0);
        }
    }
  
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "enemyBullet")
        {
            return;
        }

        if (col.gameObject.tag == "friendlyBullet")
        {
            Destroy(col.gameObject);
            Instantiate(particleEffect, transform.position, transform.rotation);
            hp--;

            if (hp <= 0)
            {
                int randomNumber = Random.Range(0, 100);
                if (randomNumber < 5) Instantiate(powerUp, transform.position, powerUp.transform.rotation);
                if (randomNumber > 95) Instantiate(powerDown, transform.position, powerDown.transform.rotation);
                Destroy(gameObject);
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCharacter>().score += scoreReward;
            }
        }
        else if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<PlayerCharacter>().hp--;
            Instantiate(particleEffect, transform.position, transform.rotation);
            hp--;

            if (hp <= 0)
            {
                Destroy(gameObject);
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCharacter>().score += scoreReward;
            }
        }
    }

    void switchTimer()
    {
        changeTimer -= Time.deltaTime;

        if (changeTimer < 0)
        {
            switchDir(directionSwitch);
            changeTimer = maxTimer;
        }
    }

    bool switchDir(bool dir)
    {
        if (dir)
        {
            directionSwitch = false;
        }
        else
        {
            directionSwitch = true;
        }

        return directionSwitch;
    }
   
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody>();
        maxTimer = changeTimer;
        maxShootTimer = shootTimer;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x == Limits.maximumX)
        {
            switchDir(directionSwitch);
        }

        if (transform.position.x == Limits.minimumX)
        {
            switchDir(directionSwitch);
        }

        checkPosition();
        Movement();
        shootTimer -= Time.deltaTime;

        if (canShoot)
        {
            if (shootTimer <= 0)
            {
                GameObject newBullet = Instantiate(bullet, shootingPosition.transform.position, transform.rotation);
                newBullet.GetComponent<Rigidbody>().velocity = Vector3.up * -shootPower;
                shootTimer = maxShootTimer;
            }
        }

        shootTimer = Random.Range(0, 100);
        switchTimer();
    }
}